<div id="contenedor">
    <section id="banner">
        <img src="<?= base_url('assets/images/conectate.png') ?>" alt="" id="conectate">

        <img src="<?= base_url('assets/images/nov9no_encuentro.png') ?>" alt="" id="nov9no_encuentro">

        <a href="<?= base_url('/registro') ?>" id="btn_registrate">Registrate</a>
    </section>

    <section id="fondo">
        <section id="fondo_blanco">
            <img src="<?= base_url('assets/images/logo_ujim.png') ?>" alt="" id="logo_ujim">  
        
            <a style="background-color: #00FFFF" href="<?= base_url('/registro') ?>" id="btn_registrate2">Registrate</a>
            
            <div id="info_block">
                <h3 id="info"><b>Info</b></h3>
                
                <p id="p1">
                    Días: <br>
                    16-17-18 <br>
                    <b>AGOSTO</b>
                </p>
                
                <p id="p2">
                    Orador:
                    <b>Carbonell José</b>
                </p>
            </div>

            <p id="contacto">
                <b>Para contactos</b> <br>
                3757-432798 <br>
                <b>(Patricia)</b> <br>
                <br>
                3764-243680 <br>
                <b>(Luciana)</b>
            </p>

            <p id="email">
                iceiguazu@yahoo.com.ar / patriyjuanramon@gmail.com
            </p>
        </section>    
    </section>
</div>